<?php 

/**
* Implementation of hook_menu().
*/
function autosuggest_menu() {
  $items = array();
  $items['js/autosuggest/query'] = array(
    'title' => 'AutoSuggest Callback',
    'page callback' => 'autosuggest_error',
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/autosuggest'] = array(
    'title' => 'AutoSuggest Search',
    'description' => 'Specify which content-types to allow for AutoSuggest',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('autosuggest_admin'),
    'access arguments' => array('administer autosuggest'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
* Implementation of hook_js()
* Defines the callback funciton/page for the JS handler module.
*/
function autosuggest_js() {
  return array(
    'query' => array(
      'callback' => 'autosuggest_query',
    ),
  );
} 

/**
 * Implementation of hook_perm().
 */
function autosuggest_perm() {
  return array('administer autosuggest', 'access autosuggest');
}

/**
 * Callback for the activetabs admin settings page.
 */
function autosuggest_admin() {
  $form = array();
  
	  $form['autosuggest'] = array(
    	'#type' => 'fieldset',
	    '#title' => t('Settings'),
    	'#description' => t('Set the content-types that you would like to appear via AutoSuggest'),
	  );
	  $form['autosuggest']['autosuggest_node_types'] = array(
    	'#type' => 'checkboxes',
	    '#title' => t('Node types'),
	    '#default_value' => variable_get('autosuggest_node_types', array()),
	    '#options' => node_get_types('names'),
	  );
	  $form['autosuggest']['autosuggest_formname'] = array(
    	'#type' => 'textfield',
	    '#title' => t('Search form CSS ID'),
	    '#size' => 60,
	    '#description' => t('You need to locate and specify the CSS ID of the search form. This will be improved later when there is a custom AutoSuggest block.'),
	    '#default_value' => variable_get('autosuggest_formname', '#search-theme-form'),
	  );
	  $form['autosuggest']['autosuggest_placeholder'] = array(
    	'#type' => 'textfield',
	    '#title' => t('Textfield placeholder text'),
	    '#size' => 60,
	    '#description' => t('Specify the text that will appear inside the search box as placeholder text'),
	    '#default_value' => variable_get('autosuggest_placeholder', 'Search'),
	  );
	  $form['autosuggest']['autosuggest_count'] = array(
    	'#type' => 'textfield',
	    '#title' => t('Limit'),
	    '#size' => 5,
	    '#description' => t('Specify how many suggestions you would like to appear via AutoSuggest'),
	    '#default_value' => variable_get('autosuggest_count', 10),
	  );
	  
  return system_settings_form($form);
}

/**
 * Implementation of hook_preprocess_page().
 */
function autosuggest_preprocess_page(&$variables) {
  global $theme;
  $themepath = drupal_get_path('theme', $theme);
  $formname = variable_get('autosuggest_formname','#search-theme-form');
  $placeholder = variable_get('autosuggest_placeholder', 'What are you looking for?');

  if (user_access('access autosuggest')) {
  	// check if autosuggest.css override is in theme folder, else use modules copy
//    $variables['styles'] .= '<link type="text/css" rel="stylesheet" media="all" href="'.base_path().(((file_exists($themepath.'/autosuggest.css')) ? $themepath : drupal_get_path('module', 'autosuggest')).'/autosuggest.css').'" />'.chr(10);
    $variables['styles'] .= '<style type="text/css"> '.$formname.' .container-inline { position: relative; width: 194px; display: block; } '.$formname.' .form-item { height: 30px; display: block; padding: 0px !important; margin: 0px !important; } '.$formname.' .form-item label { display: none; } '.$formname.' .form-item input[type=text] { border: 2px solid #999; font-size: 15px; height: 22px; width: 176px; padding: 2px 7px !important; -moz-border-radius:5px;-webkit-border-radius:5px; background-color: #FFF; background-image: url(\'/sites/all/modules/autosuggest/searchbw.png\'); background-repeat: no-repeat; background-position: 168px 5px; } '.$formname.' .form-submit { position: absolute; top: 7px; right: 5px; background-color: transparent; border: 0px none; padding: 0px; color: transparent; width: 22px; cursor: pointer; outline: 0 none !important;  -moz-outline-style: none; }#autosuggest { position: absolute; top: 26px; border-bottom: 1px solid #999; border-left: 2px solid #999; border-right: 2px solid #999; display: none; } #autosuggest .autosuggest_item { width: 170px; } #autosuggest .autosuggest_item a { color: #000; background: #fff; border-bottom: 1px solid #999; padding: 7px 10px; width: 170px; display: block; background-color: #FFF; background-image: url(\'/sites/all/modules/autosuggest/linkgobw.png\'); background-repeat: no-repeat; background-position: 168px 5px; cursor: pointer; } #autosuggest .autosuggest_item:hover { background-color: #EEE; }#autosuggest .autosuggest_item .highlight { font-weight: bold; font-size: 13px; } </style>';
    // check if autosuggest.js override is in theme folder, else use modules copy
//    $variables['scripts'] .= '<script type="text/javascript" src="'.base_path().(((file_exists($themepath.'/autosuggest.js')) ? $themepath : drupal_get_path('module', 'autosuggest')).'/autosuggest.js').'"></script>'.chr(10);
    // set search placeholder
    $variables['scripts'] .= '<script type="text/javascript"> var xhr; var active; var timeout; var theData; var query; function searchauto(query) { if (active==true) { xhr.abort(); active = false; } else { active = true; } xhr = $.ajax({ type: "POST", url: "/js/autosuggest/query/"+query, dataType: "json", data: "searchword="+query, cache: true, timeout: 5000, success: function(results) { if (results!=null) { $("#autosuggest").empty().show(); $.each(results, function(i,result){ var html = \'<div class="autosuggest_item" align="left"><!--//<a href="/\'+result.dst+\'" style="float:right;width:16px;height:16px;"><img src="/sites/all/modules/autosuggest/clear.gif"></a>//--><a href="/\'+result.dst+\'"><span class="itemtitle">\'+result.title+\'</span></a></div>\'; $("#autosuggest").append(html); }); $("#autosuggest .autosuggest_item .itemtitle").highlight(query); } else { $("#autosuggest").empty(); } active = false; } }); } $(document).ready(function(){ $(".autosuggest_item").click(function(){ console.log("this should be working, but its not"); }); $("'.$formname.' .form-item input[type=text]").css("color","#bbbbbb").val("'.$placeholder.'"); /** * highlight v3 * Highlights arbitrary terms. * <http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html> * MIT license. * Johann Burkard * <http://johannburkard.de> * <mailto:jb@eaio.com> */ jQuery.fn.highlight = function(pat) { function innerHighlight(node, pat) { var skip = 0; if (node.nodeType == 3) { var pos = node.data.toUpperCase().indexOf(pat); if (pos >= 0) { var spannode = document.createElement("span"); spannode.className = "highlight"; var middlebit = node.splitText(pos); var endbit = middlebit.splitText(pat.length); var middleclone = middlebit.cloneNode(true); spannode.appendChild(middleclone); middlebit.parentNode.replaceChild(spannode, middlebit); skip = 1; } } else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) { for (var i = 0; i < node.childNodes.length; ++i) { i += innerHighlight(node.childNodes[i], pat); } } return skip; } return this.each(function() { innerHighlight(this, pat.toUpperCase()); }); }; jQuery.fn.removeHighlight = function() { return this.find("span.highlight").each(function() { this.parentNode.firstChild.nodeName; with (this.parentNode) { replaceChild(this.firstChild, this); normalize(); } }).end(); }; /* Search */ $("#block-search-0 h3").remove(); /* remove block title */ $("'.$formname.'").attr("autocomplete", "off"); /* disable firefox autocomplete suggestions */ $("'.$formname.' .form-item").append(\'<div id="autosuggest"></div>\'); /* add autosuggest item parent */ var autosuggest = $("'.$formname.' .form-item input[type=text]"); var autosuggestVal; var firstRun = true; autosuggest.focus(function(){ /* this is to capture the placeholder text that is set via jQuery */ if (firstRun==true) { autosuggestVal = autosuggest.val(); firstRun = false; } if (autosuggest.val() == autosuggestVal) { /* search input hasn\'t changed, blank out on focus */ autosuggest.val("").css("color","#000000"); } else { /* search input has been changed, make it black */ autosuggest.css("color","#000000"); } }).blur(function(){ if (autosuggest.val() == "") { /* search input is blank, restore default placeholder text */ autosuggest.val(autosuggestVal).css("color","#BBBBBB"); } else { /* leave search input black to indicate its changed */ autosuggest.css("color","#000000"); } }); /* AutoSuggest input box hook */ $("'.$formname.' .form-item input[type=text]").keyup(function() { query = $(this).val(); theData = "searchword="+ query; /* remove previously pending requests *//* helps account for fast typers */ clearTimeout(timeout); /* should we make a request? *//* if((query=="") && (query.length<=3)) { *//* don\'t make the request */ if (query=="") { /* don\'t make the request */ $("#autosuggest").empty().hide(); if (active==true) { xhr.abort(); active = false; } } else { /* make the request *//* wait to send request, accounts for fast typers */ timeout = setTimeout("searchauto(query);",180); } return true; }).blur(function(){ setTimeout("$(\'#autosuggest\').hide();", 500); }); });</script>';
    

  }
}

/**
* Implementation of autosuggest_query()
* Gather query results and output JSON payload via JS handler
*/
function autosuggest_query($arg1 = NULL, $reset = FALSE) {

  $types = array_filter(variable_get('autosuggest_node_types', array()));
  $limit = variable_get('autosuggest_count', 10);
  $args = array_merge($types, array($arg1), array($limit));

  if ($types) {
    $results = db_query("SELECT nid,title,type,status,changed,src,dst FROM {node} RIGHT JOIN {url_alias} ON concat('node/',node.nid)=url_alias.src WHERE node.type in (".db_placeholders($types, 'varchar').") AND node.title LIKE '%%%s%%' ORDER BY node.changed LIMIT %d", $args);
  }
  
  while ($result = db_fetch_array($results)) {
    if ($result['status'] != 0) {

      // end-user doesn't need this info
      unset($result['nid']);
      unset($result['src']);
      unset($result['changed']);
      unset($result['status']);
      unset($result['type']);
      
      // prepare payload
      $payload[] = $result;
    }
  }

  // output payload
  print json_encode($payload);
}

/**
* Implementation of autosuggest_error()
* Fired when JS handler module is not installed, enabled, or working.
*/
function autosuggest_error($arg1 = NULL) {
  print "Error: Unable to access AutoSuggest (is the JS module properly installed?)";
}

?>